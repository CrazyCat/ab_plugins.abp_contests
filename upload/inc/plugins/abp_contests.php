<?php
if(!defined("IN_MYBB"))
	die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");

define('CN_ABPCTS', 'abp_contests');

/**
 * Hooks
 */
$plugins->add_hook('admin_forum_menu', 'abp_contest_admin_menu');

/**
 * Displayed informations
 */
function abp_contests_info() {
	global $lang;
	$lang->load(CN_ABPCTS);
	return array(
		'name'			=> $lang->abp_contests_name,
		'description'	=> $lang->abp_contests_desc,
		'author'		=> 'CrazyCat',
		'version'		=> '1.0',
		'compatibility'	=> '18*'
		'codename' => CN_ABPCTS
	);
}

function abp_contests_install() {
	global $db, $lang;
	
	$collation = $db->build_create_table_collation();
	
	$tables[] = "CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."abp_contests (
		cid int(11) NOT NULL AUTO_INCREMENT COMMENT 'Contest ID',
		ctitle varchar(255) NOT NULL COMMENT 'Contest title',
		cdesc text NOT NULL COMMENT 'Contest description',
		ctype int(8) NOT NULL COMMENT 'Contest type (post, file, picture)',
		allowed_files varchar(255) DEFAULT NULL COMMENT 'Allowed file types',
		pgroups varchar(64) NOT NULL COMMENT 'Groups can participate',
		vgroups varchar(64) NOT NULL COMMENT 'Groups can vote',
		pstartdate date NOT NULL COMMENT 'Start of participation',
		penddate date NOT NULL COMMENT 'End of participation',
		vstartdate date NOT NULL COMMENT 'Start of vote',
		venddate date NOT NULL COMMENT 'End of vote',
		vtype int(8) NOT NULL COMMENT 'Type of vote',
		oncal tinyint(1) NOT NULL COMMENT 'Show on calendar (yes/no)',
		notes text NOT NULL COMMENT 'Notes values / labels',
		allowcom tinyint(1) NOT NULL COMMENT 'Comments allowed (yes/no)',
		anonymous tinyint(1) NOT NULL COMMENT 'Anonymous vote ? (yes/no)',
		PRIMARY KEY (cid)
		) ENGINE=MyISAM{$collation}";
	$tables[] = "CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."abp_conparticip ()";
	$tables[] = "CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."abp_convote ()";
	foreach($tables as $table) {
		$db->write_query($table);
	}
}

function abp_contests_uninstall() {
	global $db;
	if ($db->table_exists('abp_convote')) {
		$db->drop_table('abp_convote');
	}
	if ($db->table_exists('abp_conparticip')) {
		$db->drop_table('abp_conparticip');
	}
	if ($db->table_exists('abp_contesst')) {
		$db->drop_table('abp_contests');
	}
}

function abp_contests_is_installed() {
	if ($db->table_exists('abp_convote') && $db->table_exists('abp_conparticip') && $db->table_exists('abp_contests')) {
		return true;
	}
	return false;
}

/**
 * ACP part
 */
 
 function abp_contests_admin_menu($sub_menu) {
	global $lang;
	$lang->load('abp_contest');
	$sub_menu[] = array('id' => 'abp_contests', 'title' => $lang->abp_contests, 'link' => 'index.php?module=abp_contests');
	return $sub_menu;
 }
